﻿using eShop.Domains.Models;
using eShop.Domains.Repository;
using Microsoft.EntityFrameworkCore;

namespace eShop.Infrastructure
{
    public class ProductRepository : IProductRepository
    {
        private readonly Domains.AppDbContext _context;
        public ProductRepository(Domains.AppDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Product?> GetByIdAsync(int id) => await _context.Product.FirstOrDefaultAsync(x => id == x.Id);

        public async Task<List<Product>> GetAllAsync() => await _context.Product.ToListAsync();

        public async Task<Product> AddProductAsync(Product product)
        {
            await _context.Product.AddAsync(product);
            await _context.SaveChangesAsync();
            return product;
        }
    }
}
