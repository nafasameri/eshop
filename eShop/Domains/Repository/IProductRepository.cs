﻿using eShop.Domains.Models;

namespace eShop.Domains.Repository
{
    public interface IProductRepository
    {
        Task<Product> GetByIdAsync(int id);

        Task<List<Product>> GetAllAsync();

        Task<Product> AddProductAsync(Product product);
    }
}
