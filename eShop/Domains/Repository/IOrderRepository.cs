﻿using eShop.Domains.Models;

namespace eShop.Domains.Repository
{
    public interface IOrderRepository
    {
        Task<Order> GetByIdAsync(int id);

        Task<List<Order>> GetAllAsync();

        Task<Order> AddOrderAsync(Order order);
    }
}
