﻿using System.ComponentModel.DataAnnotations;

namespace eShop.Domains.Models
{
    public class Order
    {
        [Key]
        public int Id { get; private set; }
        public int Code { get; private set; }
        public string Name { get; private set; }
        public int TotalPrice { get; private set; }
        public string User { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public bool IsDeleted { get; private set; }
        public Product Product { get; set; }
        //public string ProductsId { get; private set; }
        //public virtual List<Product> Products { get; private set; }
    }
}
