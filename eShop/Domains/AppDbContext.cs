﻿using eShop.Domains.Models;
using Microsoft.EntityFrameworkCore;

namespace eShop.Domains
{
    public class AppDbContext : DbContext
    {
        public DbSet<Order> Order { get; private set; }
        public DbSet<Product> Product { get; private set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) //, "name=CompanyEntities")
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Product>().HasData(
            //    new Product
            //    {
            //        Id = 1,
            //        Name = "test",
            //        Price = 10000,
            //        CreatedDate = DateTime.Now,
            //        Details = "test",
            //        IsDeleted = false
            //    }
            //);
            base.OnModelCreating(modelBuilder);
        }
    }
}
