﻿using AutoMapper;

namespace eShop.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Domains.Models.Product, Application.Cammand.Create.ProductDTO>().ReverseMap();
            CreateMap<Application.Cammand.Create.CreateProductCommand,Domains.Models.Product>().ReverseMap();
            

            CreateMap<Domains.Models.Product, Application.Query.ProductDTO>().ReverseMap();
            
        }
    }
}
