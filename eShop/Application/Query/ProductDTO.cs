﻿namespace eShop.Application.Query
{
    public class ProductDTO
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string? Details { get; set; }
        public string CreatedDate { get; set; }
        public bool IsDeleted { get; set; }

        public ProductDTO (int id, string name, int price, string? details, string createdDate, bool isDeleted)
        {
            Id = id;
            Name = name;
            Price = price;
            Details = details;
            CreatedDate = createdDate;
            IsDeleted = isDeleted;
        }
    }
}
