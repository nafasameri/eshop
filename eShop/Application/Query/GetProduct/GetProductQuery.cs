﻿using MediatR;

namespace eShop.Application.Query.GetProduct
{
    public class GetProductQuery : IRequest<ProductDTO>
    {
        public int Id { get; }

        public GetProductQuery(int Id)
        {
            this.Id = Id;
        }
    }
}
