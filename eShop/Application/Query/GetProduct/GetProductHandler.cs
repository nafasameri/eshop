﻿using AutoMapper;
using eShop.Infrastructure;
using MediatR;

namespace eShop.Application.Query.GetProduct.GetProduct
{
    public class GetProductHandler : IRequestHandler<GetProductQuery, ProductDTO>
    {
        //private readonly Domains.AppContext db;
        private readonly IMapper _mapper;
        private readonly ProductRepository repository;

        public GetProductHandler(Domains.AppDbContext context, IMapper mapper)
        {
            //db = context;
            _mapper = mapper;
            repository = new ProductRepository(context);
        }

        public async Task<ProductDTO> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            var product = await repository.GetByIdAsync(request.Id);
            return _mapper.Map<ProductDTO>(product);
            //var products = db.Products.ToList();
            //List<Product> productsList = new List<Product>();
            //products.ForEach(p =>
            //{
            //    productsList.Add(new Product(p.Id, p.Name, p.Price, p.Details, p.CreatedDate, p.IsDeleted));
            //});

            //return productsList;
        }
    }
}