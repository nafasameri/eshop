﻿namespace eShop.Application.Cammand.Delete
{
    public class Product
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
