﻿using eShop.Domains.Repository;
using MediatR;

namespace eShop.Application.Cammand.Create
{
    public class CreateProductCommand : IRequest<ProductDTO>
    {
        public string Name { get; }
        public int Price { get; }
        public string? Details { get; }
        public DateTime CreatedDate { get; }
        public bool IsDeleted { get; }

        public CreateProductCommand(ProductDTO productDTO)
        {
            Name = productDTO.Name;
            Price = productDTO.Price;
            Details = productDTO.Details;
            CreatedDate = productDTO.CreatedDate;
            IsDeleted = productDTO.IsDeleted;
        }
    }
}
