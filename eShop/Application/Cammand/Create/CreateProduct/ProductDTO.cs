﻿namespace eShop.Application.Cammand.Create
{
    public class ProductDTO
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string? Details { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
