﻿using AutoMapper;
using eShop.Domains.Repository;
using eShop.Infrastructure;
using MediatR;

namespace eShop.Application.Cammand.Create
{
    public class CreateProductHandler : IRequestHandler<CreateProductCommand, ProductDTO>
    {
        //private readonly Domains.AppContext db;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public CreateProductHandler(Domains.AppDbContext db, IMapper mapper, IProductRepository productRepository)
        {
            //this.db = db;
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public async Task<ProductDTO> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            //(string, string)[] Products = new[]
            //{
            //    ("Milk", "milk is white"),
            //    ("Milk", "1 liter"),
            //    ("Milk", "2 liter"),
            //    ("Watch", "cycle"),
            //    ("Phone", "pink")
            //};
            //var products = Enumerable.Range(1, 10).Select(index =>
            //{
            //    int rand = Random.Shared.Next(Products.Length);
            //    return new Product
            //    {
            //        Name = Products[rand].Item1,
            //        Price = Random.Shared.Next(0, 1000) * 100,
            //        CreatedDate = DateTime.Now.AddMinutes(index),
            //        Details = Products[rand].Item2,
            //        IsDeleted = false
            //    };
            //}).ToList();

            var product = _mapper.Map<Domains.Models.Product>(request);
            await _productRepository.AddProductAsync(product);
            return _mapper.Map<ProductDTO>(product);
        }
    }
}