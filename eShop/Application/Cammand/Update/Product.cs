﻿namespace eShop.Application.Cammand.Update
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string? Details { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
