﻿using MediatR;

namespace eShop.Application.Configuration.Queries
{
    public interface IQueryHandler<in TQuery, TResult> :
            IRequestHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
    }
}
