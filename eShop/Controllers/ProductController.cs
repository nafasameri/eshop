using Microsoft.AspNetCore.Mvc;
using AppDbContext = eShop.Domains.AppDbContext;
using MediatR;
using eShop.Application.Cammand.Create;
using eShop.Application.Query.GetProduct;

namespace eShop.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly AppDbContext db;
        private readonly ILogger<ProductController> _logger;
        private readonly IMediator _mediator;

        public ProductController(ILogger<ProductController> logger, AppDbContext context, IMediator mediator)
        {
            _logger = logger;
            db = context;
            _mediator = mediator;
        }

        [HttpGet(Name = "GetProduct")]
        public async Task<ActionResult> Get(int id) => Ok(await _mediator.Send(new GetProductQuery(id)));

        [HttpPost(Name = "AddProduct")]
        public async Task<ActionResult> Add(ProductDTO product) => Ok(await _mediator.Send(new CreateProductCommand(product)));
    }
}