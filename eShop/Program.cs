using Microsoft.EntityFrameworkCore;
using MediatR;
using eShop.Infrastructure;
using eShop.Domains.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

IConfiguration config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .AddEnvironmentVariables()
    .AddCommandLine(args)
    .AddUserSecrets<Program>(true)
    .Build();

string connectionString = config["ConnectionStrings:SqlConnection"];
Console.WriteLine(connectionString);

builder.Services.AddDbContext<eShop.Domains.AppDbContext>(options =>
        options.UseSqlServer(connectionString));

builder.Services.AddMediatR(typeof(Program).Assembly);
builder.Services.AddTransient<IProductRepository,ProductRepository>();
//Mapper.Initialize(cfg => cfg.AddProfile<MappingProfile>());
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
